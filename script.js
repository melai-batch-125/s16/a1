function getInfo(fName,lName,mail,pass1,pass2){
	if (fName === "" || lName === "" || mail === "" || pass1 === "" || pass2 === "") {
		console.log(`Please fill in your information.`);
	} else if ((pass1.length >= 8) && (pass1 === pass2)) {
		console.log(`Thanks for logging you information.`);
	} else {
		console.log(`Password must be 8 characters and above. Password and confirm password must be matched.`);
	}
}

// 1st scenario: No empty fields, 8 character-Password, password and confirm password are matched
let firstName = `Melai`;
let lastName = `Dotollo`;
let email = `melai@gmail.com`;
let password = `Ezekiiel`;
let conPassword = `Ezekiiel`;

getInfo(firstName,lastName,email,password,conPassword); //Thanks for logging you information.


//2nd scenario: least than 8 characters for password,password and confirm password not matched
password = `try`;

getInfo(firstName,lastName,email,password,conPassword); //Password must be 8 characters and above. Password and confirm password must be matched.

//3rd scenario: One empty field
password = ``;

getInfo(firstName,lastName,email,password,conPassword); //Please fill in your information.
